import nativeShare from './nativeShare'
import baguettebox from 'baguettebox.js'

var textPage = (function () {
  var pub = {
    bindEvent() {
      var self = this;
      var $c_txtBtn = $(".c_txtBtn");
      $c_txtBtn.click(function () {
        var defs = $(this).data("status");
        console.log(defs);
        self[defs]("finished");
      });
      $(".c_mainTxtContainer").on("click", ".c_mainTxt a", function () {
        window.SIMA && window.SIMA({
          action: '_click',
          data: {
            'aid': 'news_urllink_click'
          }
        });
      });
      var $adCard = $(".c_pnCard");
      $adCard.find(".c_pncTit").click(function (e) {
        window.SIMA && window.SIMA({
          action: '_click',
          data: {
            'aid': 'news_ad_title_click'
          }
        });
      });
      $adCard.find('img').click(function (event) {
        window.SIMA && window.SIMA({
          action: '_click',
          data: {
            'aid': 'news_ad_picture_click'
          }
        });
      });
      $adCard.find(".c_pncIcon_2 a").click(function (event) {
        window.SIMA && window.SIMA({
          action: '_click',
          data: {
            'aid': 'news_ad_button_click'
          }
        });
      });
    },
    hideP(ifFirst) {
      var ifFirst = ifFirst || "first";
      var $mainTxt = $(".c_mainTxt_box").find(".c_mainTxt,#comApp");
      if ($mainTxt.length <= 2) {
        $(".c_txtBtn").hide();
        $(".staticApp").css("visibility", "visible");
        return
      }
      $mainTxt.each(function (i, v) {
        if (i > 1) {
          $(this).css("display", "none")
        }
      });
      var $c_txtBtn = $(".c_txtBtn");
      $c_txtBtn.text("点击查看全文").data("status", "showP");
      $(".staticApp").css("visibility", "visible");

      var toTop = $c_txtBtn.offset().top;
      $(".c_txtInfo").hide();
      if (ifFirst == "finished") {
        console.log(ifFirst);
        window.scrollTo(0, toTop - 150);
        window.SIMA && window.SIMA({
          action: '_click',
          data: {
            'aid': 'caitong_newsdetail_close'
          }
        });
      } else if (ifFirst == "first") {

      }
    },
    showP() {
      var $c_txtBtn = $(".c_txtBtn");
      // var toTop = $c_txtBtn.offset().top;
      $(".c_mainTxt_box").find(".c_mainTxt").show();
      $c_txtBtn.text("点击收起").data("status", "hideP");
      $(".c_txtInfo").show();
      window.SIMA && window.SIMA({
        action: '_click',
        data: {
          'aid': 'caitong_newsdetail_checkall'
        }
      });
      // window.scrollTo(0, toTop - 150);
    },
    shareChoose() {
      var config = {
        url: window.location.href, // 分享的网页链接
        title: document.title, // 标题
        desc: window.shareInfo.desc || document.title, // 描述
        img: window.shareInfo.desc || $(".c_mainImg").eq(0).attr("src"), // 图片
        img_title: window.shareInfo.img_title || $(".c_mainImg").eq(0).attr("alt"), // 图片标题
        from: window.shareInfo.from || "新浪彩票" // 来源
      };
      var share_obj = new nativeShare('nativeShare', config);
      $(".c_share_box").show();
    },
    imgHset() {
      // var imgList = {
      //   "adImg1": 0.33, //单大图
      //   "adImg2": 0.57, //双小图
      //   "adImg3": 0.75, //双大图
      //   "adImg4": 0.75, //三图
      //   "adImg5": 0.75, //单小图
      // }
      var imgs = $(".c_pnCard").find("img");
      imgs.each(function () {
        var imgW = $(this).width();
        var imgH = $(this).data("height") - 0;
        imgH = imgW * imgH;
        imgH = Math.ceil(imgH);
        $(this).height(imgH);
      })
    },
    setWidth(){
      let w=util.viewData().viewWidth;
      if (w > maxW) {
        w = maxW;
        $("body").css({ 'left': '50%', 'right': 'auto', 'width': w + 'px', 'marginLeft': -w / 2 + 'px' });
      }
      let h = util.viewData().viewHeight;
      $('body').css({'minHeight': h});
    },
    run() {
      this.hideP();
      this.bindEvent();
      this.shareChoose();
      this.imgHset()
      baguettebox.run('.staticApp');
      this.setWidth();
      // $(".c_pncIcon_2").each(function () {
      //   var w = $(this).css("width").split("px")[0];
      //   w=w-0;
      //   console.log(w);
      //   $(this).css("width", w + 100);
      // })
      // $('.content').show();
    }

  }
  return pub;
})();



export default textPage;
