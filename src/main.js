import Vue from 'vue';
import VueRouter from 'vue-router';
// import VueResource from 'vue-resource';
var VueTouch = require('vue-touch');
import App from './App';

// 直接引入静态样式
import 'common/css/reset.css';
import 'common/stylus/base.styl';
import 'common/js/lib.js';
// import 'common/stylus/textPage.styl';
import 'common/stylus/baseLess.less';
import 'common/stylus/textPage.less';
window.$ = $;
window.Vue = Vue;
// 设置全局js
import textPage from 'common/js/textPage';



Vue.use(VueRouter);
// Vue.use(VueResource);
Vue.use(VueTouch);
// 引入主vue外壳
let app = Vue.extend(App);
// 引入其他vue外壳
import ComApp from './ComApp';
let comApp = Vue.extend(ComApp);
import SurveyApp from './SurveyApp';
let surveyApp = Vue.extend(SurveyApp);
import FeedApp from './FeedApp';
let feedApp = Vue.extend(FeedApp);


let router = new VueRouter({
  linkActiveClass: 'active',
  history: true,
  saveScrollPosition: true
});
// 主页js运行
textPage.run();

// 挂载其他vue模块
new comApp().$mount('#comApp');
new surveyApp().$mount('#surveyApp');
new feedApp().$mount('#feedApp');
// import ToTopApp from './ToTopApp';
// let toTopApp = Vue.extend(ToTopApp);
// new toTopApp().$mount('#toTopApp');
router.start(app, '#app');
