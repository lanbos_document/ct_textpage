# sell

> sell app

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).



1.https://github.com/vuejs/vue-touch
2.webStorm 的.idea 目录加入.gitignore无效的解决方法
  先执行 git rm -r --cached .idea
  再重新加入.gitignore文件

  在`index.js`中指定了`build`命令输出路径：
```js
		// 输出到指定目录
    index: path.resolve(__dirname, '../../publish/ct_textPage/wap/index.html'),
    assetsRoot: path.resolve(__dirname, '../../publish/ct_textPage/wap'),
```

## 线上模版地址：
### 彩票正文页：
http://cms.pub.sina.com.cn/index.php?r=template/update&id=26088
### 彩通正文页：
http://cms.pub.sina.com.cn/index.php?r=template/update&category_id=1216&type=1&id=26099

### 广告模块地址：
http://cms.pub.sina.com.cn/index.php?r=template/update&id=26063