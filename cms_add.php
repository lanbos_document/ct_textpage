<?php
/**
 * @method API_Base api()
*/
class BC_Lottery_Article_Wap_Ad_Handler extends BC_Lottery_Ad_Handler {

    /**
     * 单图:左标题右图
     * @param  string $smartdata  信息流数据
     * @return string             单图:左标题右图
     */
    public function _handleStyle1($smartdata){
        // 下部推广内容
        // 广告html
        $html = '<div class="c_pnCard">
        <div class="c_pnCards3 c_cardWrap">
          <div class="c_pncLeft">
            <div class="c_pncTit"><a href="'.$smartdata['url'].'">'.$smartdata['adtitle'].'</a></div>
            <div class="c_pncLabels clearfix">
              <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
              '.$extHtml.'
            </div>
            <div class="c_pncRight">
              <a href="'.$smartdata['url'].'">
                <img src="'.$smartdata['pic1'].'" alt="'.$smartdata['adtitle'].'" data-height="0.75">
              </a>
            </div>
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    /**
     * 单图:上标题下图
     * @param  string $smartdata  信息流数据
     * @return string             单图:上标题下图
     */
    public function _handleStyle2($smartdata){
        // 下部推广内容
        $extHtml = $this->_getExtHtml($smartdata);
        // 广告html
        $html ='<div class="c_pnCard">
        <div class="c_pnCards1 c_cardWrap">
          <a href="'.$smartdata['url'].'">
            <div class="c_pncTit">'.$smartdata['adtitle'].'</div>
            <div class="c_pncImg_1">
              <img src="'.$smartdata['pic1'].'" alt="'.$smartdata['adtitle'].'" data-height="0.33">
            </div>
          </a>
          <div class="c_pncLabels clearfix">
            <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
            '.$extHtml.'
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    /**
     * 单图无标题
     * @param  string $smartdata  信息流数据
     * @return string             单图无标题
     */
    public function _handleStyle3($smartdata){
        // 下部推广内容
        $extHtml = $this->_getExtHtml($smartdata);
        // 广告html
        $html ='<div class="c_pnCard">
        <div class="c_pnCards1 c_cardWrap">
          <a href="'.$smartdata['url'].'">
            <div class="c_pncImg_1">
              <img src="'.$smartdata['pic1'].'" alt="'.$smartdata['adtitle'].'" data-height="0.33">
            </div>
          </a>
          <div class="c_pncLabels clearfix">
            <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
            '.$extHtml.'
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    /**
     * 双图
     * @param  string $smartdata  信息流数据
     * @return string             双图
     */
    public function _handleStyle4($smartdata){
        // 下部推广内容
        $extHtml = $this->_getExtHtml($smartdata);
        // 广告html
        $html = '<div class="c_pnCard">
        <div class="c_pnCards1 c_cardWrap">
          <a href="'.$smartdata['url'].'">
            <div class="c_pncTit">'.$smartdata['adtitle'].'</div>
            <div class="c_pncImg_2 clearfix c_pncImg_2b">
              <div class="c_pncImg_2l">
                <img src="'.$smartdata['pic1'].'" alt="'.$smartdata['adtitle'].'" data-height="0.75">
              </div>
              <div class="c_pncImg_2r ">
                <img src="'.$smartdata['pic2'].'" alt="'.$smartdata['adtitle'].'" data-height="0.75">
              </div>
            </div>
          </a>
          <div class="c_pncLabels clearfix">
            <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
            '.$extHtml.'
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    /**
     * 三图
     * @param  string $smartdata  信息流数据
     * @return string             三图
     */
    public function _handleStyle5($smartdata){
        // 下部推广内容
        $extHtml = $this->_getExtHtml($smartdata);
        // 广告html
        $html = '<div class="c_pnCard">
        <div class="c_pnCards1 c_cardWrap">
          <a href="'.$smartdata['url'].'">
            <div class="c_pncTit">'.$smartdata['adtitle'].'</div>
            <div class="c_pncImg_3 clearfix">
              <div class="c_pncImg_3l">
                <img src="'.$smartdata['pic1'].'" alt="'.$smartdata['adtitle'].'" data-height="0.75">
              </div>
              <div class="c_pncImg_3m">
                <img src="'.$smartdata['pic2'].'" alt="'.$smartdata['adtitle'].'" data-height="0.75">
              </div>
              <div class="c_pncImg_3r">
                <img src="'.$smartdata['pic3'].'" alt="'.$smartdata['adtitle'].'" data-height="0.75">
              </div>
            </div>
          </a>
          <div class="c_pncLabels clearfix">
            <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
            '.$extHtml.'
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    /**
     * 无图
     * @param  string $smartdata  信息流数据
     * @return string             无图
     */
    public function _handleStyle6($smartdata){
        // 下部推广内容
        $extHtml = $this->_getExtHtml($smartdata);
        // 广告html
        $html ='<div class="c_pnCard">
        <div class="c_pnCards1 c_cardWrap">
          <a href="'.$smartdata['url'].'">
            <div class="c_pncTit">'.$smartdata['adtitle'].'</div>
          </a>
          <div class="c_pncLabels clearfix">
            <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
            '.$extHtml.'
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    /**
     * 双图:开奖直播专用
     * @param  string $smartdata  信息流数据
     * @return string             双图:开奖直播专用
     */
    public function _handleStyle7($smartdata){
        // 下部推广内容
        $extHtml = $this->_getExtHtml($smartdata);
        // 广告html
        $html = '<div class="c_pnCard">
        <div class="c_pnCards1 c_cardWrap">
          <a href="'.$smartdata['url'].'">
            <div class="c_pncTit">'.$smartdata['adtitle'].'</div>
            <div class="c_pncImg_2 clearfix">
              <div class="c_pncImg_2l">
                <img src="'.$smartdata['pic1'].'" alt="'.$smartdata['adtitle'].'" data-height="0.57">
              </div>
              <div class="c_pncImg_2r ">
                <img src="'.$smartdata['pic2'].'" alt="'.$smartdata['adtitle'].'" data-height="0.57">
              </div>
            </div>
          </a>
          <div class="c_pncLabels clearfix">
            <div class="c_pncIcon_1">'.$smartdata['tuiguang'].'</div>
            '.$extHtml.'
          </div>
        </div>
        <div class="c_8block"></div>
      </div>';
        return $html;
    }

    private function _getExtHtml($smartdata){
        // 下部推广内容
        $extHtml = '';
        if( !empty($smartdata['tag']) && !empty($smartdata['tagurl']) ){
            $extHtml .= '<div class="c_pncIcon_2"><a href="'.$smartdata['tagurl'].'">'.$smartdata['tag'].'</a></div>';
        }
        if( !empty($smartdata['tag']) && empty($smartdata['tagurl']) ){
            $extHtml .= '<div class="c_pncIcon_2"><span>'.$smartdata['tag'].'</span></div>';
        }
        if( !empty($smartdata['author']) ){
            $extHtml .= '<div class="c_pnclTxt">'.$smartdata['author'].'</div>';
        }
        return $extHtml;
    }

}