<!DOCTYPE html>
<?php 

$vt = 89;

$doc = $api->current()['doc'];
$docId = $doc['_id'];
if( empty($docId) ){
  header("Location: //caitong.sina.cn");
    exit;
}

$cacheKey = md5("caitong_wap_chartv20170504_{$docId}");
$html = $api->cache()->get($cacheKey);
/*if( $html !== false ){
  echo $html;
  exit;
}*/

$doc = $api->doc($docId);

//非上线状态文章
if ($doc['online'] <= 0) {
    header("Location: //caitong.sina.cn");
    exit;
}

// 获取相关必要数据
$tags = $doc['tags'];
$summary = '';
$title = $doc['title'];
$stitle = $doc['stitle'];
$docID = $doc['_id'];
$oriSource = $doc['source']['ml_signature'];
$createTime = $doc['cTime'];
$allCIDs = $doc['allCIDs'];
$pic1 = $doc['feedPic'];
$comment = $doc['comment'];
$mainPic = $doc['mainPic'];
$url = $api->current()->url;

// 生成meta title
$metaTitle = !empty($stitle) ? $stitle : $title;
// 生成keywords
$keywords = implode(",", $tags);
// 生成description
$desc = '新浪彩通为用户提供全面及时的中文彩票行业资讯，内容涵盖事件观察，行业动向探讨、国际彩票剖析、人物采访以及彩市新闻分析等深度的彩票行业资讯';
if (empty($summary)) {
    $summary = bc_helper::getContentByDocinfo($doc,true,true,true);
    $summary = BC_helper::msubstr($summary, 0, 60);
}
$description = !empty($summary) ? $summary : $desc;
// 生成媒体名称
$source = BC_WapMediaSource::getFormatSourceDate($oriSource, null);
// 生成评论ID
$commentID = "{$comment['channel']}:comos-{$docID}:0";
// 生成新闻时间
$docTime = date('Y年m月d日 H:i', strtotime($createTime));

// ================================ 处理新闻内容 ========================================
$contentBaseArr = $doc['content'];
$contentMTArr = !empty($doc['_extra']['content'][$vt]) ? $doc['_extra']['content'][$vt] : [];
$contentBase = '';
foreach ($contentBaseArr as $bs) {
    $contentBase.=$bs['content'];
}
$contentMT = '';
foreach ($contentMTArr as $mt) {
    $tmpContent = $mt['content'];
    $contentMT.=$tmpContent;
}
$contentMTTmp = trim(strip_tags($contentMT));
$content = !empty($contentMTTmp) ? $contentMT : $contentBase;

$contentHandler = new BC_Lottery_Article_Content_Handler();
$content = $contentHandler->handleContent($content, 'caitong_wap');
// 处理如果没有主图情况下的第一个div标签的间距
$content = '<div class="c_mainImg_box wrap15"></div>' . $content;
if( !empty($mainPic) ){
  $content .= '<div class="c_mainTxt"><div class="img_wrapper"><img src="' . $mainPic . '" /><span></span></div></div>';
}

// ================================ 处理新闻内容 ========================================


// ================================ 处理广告 ======================================
// 取出正文页配置
$cfgTpl = $api->userConfByPubport(130833, '广告维护', 99);
//var_dump($cfgTpl);
$adCfg = $cfgTpl['caitongWap']['value'];
$adHandler = new bc_lottery_article_wap_ad_handler();
$adsArr = $adHandler->handleAd($adCfg);
$adHtml = implode(PHP_EOL, $adsArr);
if( !empty($adsArr) ){
  $adHtml = '<div class="c_16block"></div>' . PHP_EOL . $adHtml;
}
// ================================ 处理广告 ======================================
?>
{{block var="html"}}
<html>

<head>
  <meta charset="utf-8">
  <title>{{$metaTitle|escape}}</title>
  <link rel="icon" href="http://n1.sinaimg.cn/sports/sina_lotto/caitong/wap/i/favicon64.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="http://n1.sinaimg.cn/sports/sina_lotto/caitong/wap/i/favicon64.ico" type="image/x-icon" />
  <meta name="keywords" content="{{$keywords|escape}}"
  />
  <meta name="description" content="{{$description|escape}}"
  />

  <!-- Mobile Devices Support @begin -->
  <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,user-scalable=no" name="viewport" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="format-detection" content="telephone=no, email=no">
  <meta name="msapplication-tap-highlight" content="no" />
  <meta name="browsermode" content="application" />
  <meta name="x5-page-mode" content="default|app" />
  <meta name="screen-orientation" content="portrait" />
  <meta name="x5-orientation" content="portrait" />

  <meta name="referrer" content="always">
  <meta name="sudameta" content="allCIDs:<?=implode(",", $allCIDs);?>">
  <meta property="og:type" content="news"/>
  <meta property="og:title" content="{{$metaTitle|escape}}" />
  <meta property="og:description" content="{{$metaTitle|escape}}" />
  <meta property="og:url" content="{{$url}}" />
    
  <meta property="article:author" content="{{$source}}"/> 
  <meta property="article:published_time" content="{{$createTime}}" />
  <meta name="publishid" content="{{$docID}}" />
        
  <!-- 搜索引擎 辅助抓站 -->
  <meta name="robots" content="index, follow" />
  <meta name="googlebot" content="index, follow" />
  <meta name="baidu-site-verification" content="rTnMzcp5WP" />
  <meta name="shenma-site-verification" content="0c910b838092fac1fb183f3de2eeb30a" /> 
  <!-- Mobile Devices Support @end -->
  <script type="text/javascript">
    //公共全局配置文件
    var globalConfig = {
      startTime: new Date().getTime(), //页面开始渲染时间 ， 目前应用于：日志统计、性能统计。
      isLogin: false
    }
    window._preventTanChuang_ = true;
    window.shareInfo = { //分享相关
      desc: "{{$metaTitle|escape}}", // 描述
      img: "{{$pic1|escape}}", // 图片
      img_title: null, // 图片标题
      from: "{{$url|escape}}", // 来源
    }
    // 评论相关
    window.conf_comment = {
      docID: "{{$docID|escape}}",
      commentId: "{{$commentID|escape}}",
    }

  </script>
  <script src="http://mjs.sinaimg.cn/wap/public/suda/201605101615/suda_log.min.js"></script>

<link href="//n.sinaimg.cn/sports/sina_lotto/ct_textPage/wap/css/app.css?v=a19c" rel="stylesheet"></head>

<body>
  <div id="app"></div>
  <!--正文开始 cms start-->
  <div class="staticApp content">
    <!--标题模块-->
    <div class="c_tit_box wrap15">
      <h1 class="c_tit">{{$metaTitle|escape}}</h1>
      <div class="c_titLabel clearfix">
        <span class="c_author">{{$source|escape}}</span>
        <span class="c_time">{{$docTime}}</span>
      </div>
    </div>
    <!--主图模块-->
    {{if !empty($pics)}}
    <div class="c_mainImg_box wrap15">
      {{foreach from=$pics item="mainPic" key="k"}}
      <div class="c_mainImg">
        <a href="{{$mainPic.imgurl|escape}}" data-caption="{{$mainPic.note|escape}}">
        <img src="{{$mainPic.imgurl|escape}}" alt="{{$mainPic.note|escape}}">
        </a>
        <span>{{$mainPic.note|escape}}</span>
      </div>
      {{endforeach}}
    </div>
    {{endif}}
    <!--内容模块-->
    <div class="c_mainTxt_box wrap15">
      <div class="c_mainTxtContainer">
        {{$content}}
      </div>
      <!--<div class="c_txtInfo">
        <span>南方日报记者</span>
        <span>朱小龙</span>
      </div>-->
      <div class="c_txtBtn" data-status="hideP">点击查看全文</div>
    </div>

    <!--分享模块-->
    <div class="c_share_box wrap15 clearfix">
      <span class="c_shareLabel">分享到：</span>
      <span class="c_shareFC c_shareBlock" data-shareId="weixinFriend">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACgRJREFUeAHlXE122zYQBii52+QGVU9Q9wRR1n21lNheRz1BnRNUPUGcE8RZx4oV53Ud3SDyCarewNnWEtHvAwkF4i9AQvIf30sEAoOZwcfBYDAELcUtXsOL4dO9jvxZqGhfSdUTQu5THalQlhL31qXUQkm5SGrUXCqUZTy/Wamr6YvptUW506LcqTQIO7p8MQBEfQDQF1JowFrroMQcD2AmhJqdH1x8as3Pg8FOADz6fATA4ldKiaGU8qmHft6kSqlrKcVUyej9+W/nM28Gnh22CuDhp8NXkVDj3HT0VLIxOaZ9LOR4Mpi8b8yjpmNwAOnXut3oDxmL0a0Blx00/WckzpbL+G1ofxkUwMPPh8MoVm/uDHAFQMaRfD35bTLNNjW9DwIgrK7X7XTegVm/qSI77je9Wa1ewxoXbeW2BpBWJ2P1btuLQ9uBZvtzsVGR/L2tNUZZxj73R5cv30RKXNw38DhG6kzdOQafMWdpG1mgDoCjzpdgcVxWq13fI468iVfPmyww3gA+OPDMw2oIotcUPv57uN+Non8ejOUZ8PiLXRHHxjHa1XVlZwvU8R0E3Ed/VweC3c7FZRnHP7lOZycLNNP2oYNHIDnGPfh3jtkGtqzsBCAZPshpW4YKprMec1m7VV8L4PGnw9NHBZ4BByDqsZv7kt9KH6i3ZoiVSvo+iupYihdVwXYpgPABPaxKXx+D36uyhHRR+aVs21c6hfXeFg61ivljaNOLSqdTulsptMDbmbrqCgnXKZKhC9GJ5x9+nc7tB8SkrBLqKfbd+6DpY718Zrdvu4zXCc+LErQ5AJOQJfq6q5QUQHu/jFfjsilSBkyS5VZfhBLfsMg9KaMLVo+c4k0ccypf2zxzUxh+72Tb4MGS/sW/13rwQg32fhCNXQUyKkO8C/mdPO2BBS9LiZRdNMryzQEIk3yVJQp6r8Tb5SrePz/4eCq6q77E01LL6IvvFsrW6cPBxzPw68Hh/2XXhy5LJf7I8twA8Pjy5Whr1oepRj/yYTA5MdNA+7lAIHJg54OPY8pILDs71AD3sEKNkcVqA0AI/tNqC1bU0wtAFTnh4CDiTRxSUz1M66tgA7AZZTBaA0invB3rU1ecstlV1dYpNIi08JtV3N8KiLBCjVU6gDWAIlYje1BBylwhO/HITNkqnhpEqU4Yd7X1iZSjZUL2VqazhZUGMMk8qEHVAJu0xZEYVVlelicXA66ooUCk7GSVzkpqe/8dKw1gp9vpU+m2bDf6Y7Wt2kNu0Fo3oUHUfhe6WCJaF4kVNxtkpAGMYtFvzdViwEUDjnxsVXkVQ4NIXULHiQYzDSBGF3RbhFiSO4trL9QyxCFBpC7UKSOi7a3GLNL+T4r9ttzW/bFwJINf1zQu2CCKZXSR+Gp/drqfkrkg2J+T1QOYkW8X177AhjTYhX3p8eVhQIapZggf0lS71+tHDnKdUQ+8byZ2XaFiWB8MPMRF6xtMnjZhlWa+q60ET92A6CLDBo9JC9ldnYpV56tLXycaYBfhoGPPidiFSKq5C1mWJtlC5veZWTp9n4KIRaHyQWXBOx9MdEgVcjEhdhHO08ICw1yKx249r2RvKd95dQOIPI9T1cdMW1oewVvTKjlbl9sWgJ1ZhduySvp7WmAj8FJNq+JW8H3HF2E58NjXU8dUXOkPDpCKH0tbfRtk5DyF24BXpZYGT8hRIXjs6KFjlRzdpuQTTGHZqyV0JFgulwsX0iT35zltHRjXggce0NH5IdeKhJUHncIIWBd1QgkekwWaLuAWywU8ymwb4GfHFxTALPPsvQFPJwvg3JlczdI0uXcFrwnvuj7dOoJQ7QgrerQ8A97GythCCFZjnslGJKGukJEetWDVqCsOaYZ7GcPYq0iLNCbTJ1lLnXtRR5c6+CGClyRQ6zvwQdZTuVKoqwh7kIUreR2d3hZmiFLw9OGk4OBpWQl4rr4NOvYyKra5vQ7rA/W28Ls+dw08rVlGx+/aNishDpTzZl0LeqnkY0HTsodPH0oDWkPEX27yvS8/y1uzz+i4rm9SAHYRPtJbNOlb1EcK9czUJyujGLpMW6b+6cdM3/rfhuCBsa1jvZxqCmLX1ZE5RhnkQlCug+RVhKxKxW4gIwwv1/EWbdXf60Qz9Ps505y7hbZf+DnZ0eeXubbaCiV7tTSuBNjVdBmZY6q5dqmlaxqqcBGAz3QCEYCfYNrf+kXsulQc0w3Tp/7Ju2jcJs5bgxh1FgivlOzGz33e6lXpxwWNp/CrkhBV/fNtyYfeySocMsUDSTjtMssLdKvR4UjA4x5GKg9NhQMPXFPMNIBw4jMjKMQvXOobPvGmvLTVBQQx1aU62+2prMFMA6jf3zYKJYql8knrEKa42ak2JIjUJaz1iW/mnfc6kIZPnjqNzJ1oaF4+13UpOnTEPiFATHUY1ung025jtQZQRPLMh4kLLdPuSe7PhbqYpg2IlF2X+i+WWlNrYbUGkFYQMrFAFThtGNa08Yfk0wREyjQhFXmEuoiRPWPWAFIAEgvjUIIMH+0P8bnELi2RssKGLGY0eYw2AORJgNBWqEVjh0JrcPWJ39XdLLlYoj7Nn+YdN3u3vyM22VMXGwBqEUqctReV50BL5Bfi3CNjevXyFG41Nohi2SEvHS7xl1+f468e6aStGzdvqtNsD8zazYuK4DT6HNulHzdbQt+pM/5xHA74w8Ekp0edNO0Slp0Z6P6JhThFWmkc8gVZVj6tjydtdaBvNeYskARKYq+59UuOCF5TMTf/iWu8471gugxZ4bNtgqd1lFHhSdvSJ390eThF46DpAH368Xs0ADAHIDNsgWH9AtmZzT8qdnz54lmsop6MVE/G+DYEwPnIaEOLuO/T+cFkWMSjFED6KRyPwKB28BVQkWZ3pQ47NBzQ5NRdFKmUm8KGiB2SRKepeZy/xKAMPCJSCiAb0z3yW5Yf5eVwzrt0CtuAIfSYI4SszRTbfe5/WV0h5qv1s5UWaEDY2kcrRsCd+3V/z+xkgRwf40MsKosHv6gki0aP4ZzLc3WyQDLSDJHkxLuIby6M7yON3sZijK7gcYzOFmgA0Zbo+PbM9Lkfv81elXoDSDAeHojNwCMWjQBkR15OJ+sT0rv7P0KVNsfsWgFIVJiiwmdPZ/ducYEvZ5Bs3m00fcLOi0iZACrArQ73i2U0d62eulLntuBxXK0t0AaH1ogMy+n2U2G2VPeyXmWRVbFT8u69iymDAkgRXGDSv25xcleAJHAY6BgbgqlPiFIM2WZtcABt9tgCjjBdxrcFpAEum4a3dWxb3iqARjn9NwbwmTyEMY/3xNRv5ReLAx7alK9pQ07VMl13AqAtPF21+0ie9uGCAyUocLYQZ1V43CLEwmDrW1feOYC2Qtpf6s9t43390WP63R4sqJed9ul0XOj+OBmqD4bifB6PmIX2a7aOdeX/AeNa1w823GsLAAAAAElFTkSuQmCC" alt="">
      </span>
      <span class="c_shareWX c_shareBlock" data-shareId="weixin">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACrdJREFUeAHlXEt227gSBUh5HPUKoqzA6hW0Mn4nsfwbR1lBlBVEWUGcFUQex3Zk57xxyyuIvYJWr6Dd40hE3wsKFkTxB/4sOzzHJgBWAYWLqkIBBCXFA179b/32ji93hfK6SqqOELJLcaRCWkrkrUupmZJyFpaoG6mQlsHNz4W6nexP7izKRpOyydYIWMuXfwCiHgDoCSk0YKVlUOIGAzAVQk3nC3XdJKCNAHj0/QiABW+UEn0pZbs0YCkVKKXupBQTJb3Ts1dnALXeq1YADy8P33hCjTbMsd4+rWqH2QdCjs73zk9XhdWmKgdQm2nLeycDMXgw4KIY0X96YjyfB5+rNu9KATz8ftj3AvVpa4CLATLw5PvzV+eT6KOi+UoAhNZ1dnz/E4ToFxWkYb7Jz8XiPbRxVrbd0gBS62SgvtQ9OZTtaJSfk43y5Nuy2uhFK3bJH10dfPKU+PbYwGMfKTNlZx9c+hylLaSBOgD2/D8ri+OiUjWdRxz5M1i8LDLBOAP45MAzg1UQRCcTPv5/v9vyvL+ejOYZ8HjHqoh9Yx/t4qx0bg3U8R0aeIz+LgsE+zknl3kQvMhrzrk00JjtUwePQLKPO/Dv7LMNbFI6F4Cs8EmabRIqMGfd56TnVnkmgMeXhye/FHgGHICo+27yCfdUH6iXZoiVEnh/ieJAiv20YDsRQPiADmalH7+C30vThOWk8nvSsi/RhFu+38DyTN1iE/Qaf7hv56UnlXCdHytgrAZWbrpK/KukmEqhsIgPpkmjSQkZh6mF1xNKdiFcH/73WazkDRcmmfIGgGHI4v2oZkuK2iXGX19fjIv29/jqYABe/PFVwANe2FP8GQQ05TtbipadYRp+b1gePHWLLfXh2avzabR+1/wS/PHytcAJgNx1raMSeik7Ld8boC7IsLo2NPD48gBLNdlZkbil4HQ/nu1djNy48lNj92QohSy1g5K/tQgltPDr3sULu3RtEtHmUhQ8+DnhL36vEzwKfvb64oTtCLbX9AVsli7lvuU1ACHUh/snLgl2prXoff3f5MaFrSgt28H2U/dBQIxgdA8gfUxh020QPAO6nsnRbuMgQgs1VktBVpNIoAYIGZwv7fMyNC8MTeRzvPS+TQth7MaPr/b/UEK003ioiQi5BtxZtnlrTxMrIaZs5x6yo8uDf1xXHUqov+GTOqwo7mJIhID8Gxrpmec4nvEy7YV3GEatNi+4EpCt4GWae4BfmqIrjYU5lAm+/jf2SZuwfjGEbRzTybx3gDFMo/Vbfs8GT9OGo5fIRh4Ma9cQ6EGd+wOTj7sjOE99HsdTpowyETPWoQH0gpWG5K4YE0faIpv1INy4i9YHQDfKbBrPW8zsPNPQ9FQeugWY+2WUr868wUwDiIaKqP84S0CaKn2koaPJY/Y8Mfm4uzZVJT6vnqlb7BCn8pCWy8QVTyMpjZnUPsf3/3FtMsuXRetDO528E4jhTeIJZZa70Lo2Iocu6WUAbZfrqwRTT113vJz/rYUL8RREcbykN79zYXEFj3XbPBo0z3+DTYm+8avQumvYN2xc3kC7kZWnUp8zBLANLPmIXUuoACMIkRyvtFnRsapUcgLXwmElEYghzgD+i3OFE5wo+Jg2k7NCau+O7/UUziHiuNub1EaKPgR2LQjUKYBf0Sad+HR0gGMjDJYBwnuXXZ2l9o7R4BhgjlqeP6oaSGLn4Tyt9iFOPWuA+Oj7wQd9bETI0/ki6LqAFxWPYJ7tnQ+4huZEFn1eOA/szCzsXAdNxJkpJwNf5mBSeM9NzK9750MA4ORvk5qh2+FgwOWfJtG4luMAqXjuykR6BLy1aG64IyTewTKGWXFmEbk5GNTGSkBU8hlMWHaKCIITqP0ifGk8oVbLL5hW35Yx2bQ2zDMNYtngGyumwiaMTu6hw20jUBV3HtJU6FTd4BlZ54vFgBOUyRe5r3ZjHLm5HtTb/0KMHFljyfUWkVJ9dOpFLAEKOWDmlASAxicNi/0k/whXAE2WAEjNRCvYjwu7yAu6IehAW+wqoYG6wXdVaSFOufbpl9CpWVJXwvc14UaDFKK3HMAN8nC/DuDxgotSC3+k0zH/tLaX0EJECsWnda2F2K6KkatAkdrDSfpJGiMC6Bv7eTRvns3n85lJ8y6VWMvbz5bpcUxZZhGx8zCSs0zKFAJqQmguKUQZj/SZPGhK1qzL5wxtuEHBtXgSvdZixnx6IwMTEkKhNBEwEKkDl8RL7Ar7wPVK5QCB7+zs1cXH9fJ8ORW02kIEf+ehXoKW2eGlz7vJUyc1FhNYHtINGsSB62axQZGzAMuaUVFNxGdgHY5mzqYqJ0vzu6mNATsPC/QKBZcDrCJ+uE4smDw6qYJu6UNih0Day6Xmuftgbcfn5eFkABB389JXTec64PftAzsP9l8pgAyEGV/dN5Ijwa1/zug5SGsh0e9hCtRM7OB6cCLq6gAgVnXmZH0ZhtHlvtwHJWQH8cQUH1NP4z6SPr46VEknoChjndfR5eHYfatL3SKGxIYqLyWn2BOsxITwhmzCKg1wGJgB83qk+JE1UjuY8AAYi9G2uEFRl0nEawPcND/zTVyU0x08SEbMcOmVSOCFL4lZUOai+YK/zdkYYcFfBrzUOi2fCZC5vu6k0lf8kButRao0mGkAdWxVYjljBIB57uYGzjBF7jwZGymqLRu+Dy+w3Q+sTBCvAaSE0J7ypiNlh3WVuaCFPR5hK1NHHl6ufviVaR7aKI2N1T2AwpPjKOFD5Xn+D25gUFf7+qzO3Puz8MxvYaV9uxEUIz+D8M9N/uHv6zN6FfKUBY8bCPZ5oJUGQjqgOapCyOrqkF8wW3/DxNKuok69abHwS326EcVoTQMp5PZpIfwzTkMhXjgp+6MRYWjF6KDYFdU+1rIJ4OXBCL7hQ7Em6uUikIjZJujIJOsHdvT5Qsm407u2X8KXWzRsupQNAGkuOI2OJco2+cKEgdG/WBSe9kJH7jA7tuN+Ngrl3P5/y12X5bd/7xJqTCym9vGVaHSZugEga2B8hK3qinaaE2V6gAdqjEavYXhfXBtPOkwVCyArP7o6nODhnmtD204fugG3jQto8OXZ6/N+XN/WZmGbACo/LPvKz65vW9LOsR9WHRqLhA4kAkh/gfXeIIHvlykmBsQiqcOJAJJhuUb+nMT85MtxUtaseZP6mugDbYZyU79d02NKh/t9WRKnaqBhxh5fD+Hsrck//bu6Dfuc3dNcGshqGB/iWMUMofez7GofL0VSvJfUo1waSGYdQD7Ep1VJktdRjhlX+kE/GiynNZUbQFbCl9X4TKHzNM0ZZou+xR1CSgMwtwnblWhz9r0pIvpdu/zxpkOf56J5pq+FADTMRdeVhn8r7ghVss7OpMlZCkBWrNfNgRg/uskF/o5BclaclwYenzn5wLjKKAA/fuZ6Me75NpZRVspcFjz2rbQG2gDpt1xKnWzrVhhDFBxlGdj7g7b8RdKVAkgBlvuJAySH2wKkBg6/tqF/b4FCVnhVDqAtG9+swVxGDwUkgUMHR3UeWq8VQAOmPrOMD63RWL/2yQaTAwZtwte0VZqq6Uv03giAdqPLWbuHgzC96uJIrNNxVoXHLaqYGGx5s9KNA2gLpP2l/tw26IYfPYbf7UGDOlGzX5rjTPPz81YeDMX5PB4xKxIA23KUSf8Hqv2O+J/OvEoAAAAASUVORK5CYII=" alt="">        
      </span>
      <span class="c_shareWeibo c_shareBlock" data-shareId="sinaWeibo">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACtpJREFUeAHtXNtx20YUxYP/oSswUoGZCkz/ZiKbrkBUBZIrEDsQWYGoCsxYnvyaqcBwBaErCPnrEYics8DCF28QWFKkFMzQ+7q7uHtwH7t3V7atR3zC0ahvBcGrrW0PrDD0QssakB3btj0k/MlnFYbhihW2ZfkgWjlh6Fuu+81eLNaS8JB58HK4Jwbs9dayhgBgiDcrwAxw4OMDLB3LWgLQvw8J6EEADP/4YwgpOwdoI4DVNwBY1RBrgLmAdN7Znz8vqwhNtO0NQEpbsN2+wwsmYNQzwWyLMVYwC5Pep093Lfo26mIcQAK33W4v8fYr/PYtbY0mCaI1flPHcWam1dsogOHZ2QiqegNmPfyO8aHj+WDf3y9MMWcEQEidFwTBLbzn0BRj+xwH3nzpuu4FpHHV9T1wXN2eh7OzMVT266mAx9mSV/JM3rvNXi2p2g2hbF0Q3ICbzky048BQrzCcO677oa1tbKXCsaP4gikMDE3jsYfx4WDetAFxZxWmvYP4PyXw+PEGnBPntuuX3EkCw99/H2x7PYJ3LMuTXedbR792Hh7e2H/95dcR6vbGAD4D8DQma6jzb009dCMVVjav17vFG56q5GnwmHIj8JFzlpVl+VoJVOA9PZtXhoesb+RYaiVwy6XK0/G2EqC6/CCeeyVdJYBqoXnq67zK6dc0Yu51i+1SFaZL52odr2hkC2pYOeXmSqdSKoHc2/4Pnvru/RiLQiEoBJBRlVPa2xbOzGAlsSAmRUPmVDj2ulRdr6jDM65bxevDtcQgJ4Gwe1cg8CTR/3mFAH0CsUk9KQmMpe8fUPRTVMdbuMPWayq3XtwxBa47AMs0Q+8Ms752P316IcdMAUiXjZfSeZzEA5V6URVBiVcSE0zm3NSEEIy96N3fz/V4KRUGeNe64STSHz+8Kj65n4XEjBHGfw+6TRVt07YsRokExucZH5sOdCR0PMKch5HNVmYHE/KdILiTak1e42DIEtlfWO7yONvtG31kmgAYvH07x6DGRL01g2H4DVFuD/07TVSdewTBBwmkOp92nC+tefvZ8Y6SzaIC8JGdB1WLB+ELeVpG+4UF7LShI9gAMB+T8QD+S04sftYY90KOC0GZoO1aE7RMuTv5lfY3soFBMMRAj+F5Z2DE49eUk+SklP1y3TGylbYLwP2J/n0Y9qF7f+9xYuhzhx+fPo5Zb6m+UdGy0D7X+Q4p7/QM2V8BiJeoAisO8kBNsfz4DRO/qvKibKMqVvEECfVluwIeHwT9LuL6PqLoiW1nO+o1wLLrTnmNmfbCr3fq3Y14BkkZSNtUNVwWoALay4e3b8+hmpdS0uKlhgbKk1uxuo9S8I6iKoWZQ/uH1kTEiygN1dFOXVDqdhkPfYY5ekgw6v/mD222bVlzpFNI2tfg7OxW00NdJzqvJYZlHKovdX2HdEDsbIOeqYqXDVR22FTq5ECQrH9RBkZ5R6Pp4jksUFaem+ZBvwv916wn2LSTug/qQ51vm3I509s6zrDtAA37tQaP44PJ91av51faSlxjA4gjzOWL4slx+po3AOfDDCh103WmUkj1gDYweZmpgeU4mMCVlgZZ3zTPBWsVeHoctbANw++6rNN9gafGt20Py6RwoF9mOsXYqX2j6fFz49k21d2CZJwzhZpeM+UDIJcqg3+ks9F1bVJi12vTsWGfmdx0N+zTmizeDETCgLMMgDeWg0FSfF3euu5Q57umPXyZl10HyfWHKuHCziRXX1DBHYe+aB5rgzQpa/DnY/K8+/ytSpWxa2H4quANURWk8hrv8q0fP7i4viwl3KGB2NkmvFH2nXKznW1jOZYWTmKEXyQ1bKh5APDCDcOZ3shLcn4IBDwnqOOYyhvL9n3l9wFgstHOMq2AC4JrGKQx2qSkZUkrywrI6ILkOksYf5wr1Cf2L0tjsmwcwDLpi8NJH8G8VzgBLo6ju8xc9PUB8qtCup+VK6z33pd5+Ph9S5DvVRrNAgjbxw39zzlGuTjWeItSVuoYKJhygx/vUZOuQiXPk8p8pvL6xSE2CQ4Ckt/zfLWsse15tqeShOiYIA1eFFAYYms3yYLHMViHNhkUyA7NMu/1UV0Ln3ht+K2w0UQlnSXUZmViLI4B9V1mx8IBzw3q0uAhRAUv3WhrlwkKZIdn+bKoMqmz7UWSN5whdjoaY2boXm8lB6L0wdUPZR3zWJaMy5Yk7ENHIPtAxUulDHR9qrukP2Te4TrL1AuzqojoyCg3NsReBk85edjIdxoERlSgllPZLwab9rLN02/TqUkfYkcbuGpC3ISGRruODmKffDAeowKsf7CwXSD9yv6MpJRI3L9lY2c/XIbudaZsrgjsetziYAJGBg1s20sNFIZrFa0Tlfhqa11EnvZRP31GhtzPnye6QqdUaQDs6XImvcuUk6Lywjss1JOODTPEzmGoqCF9E7JzSYTjxaUsM49FsCfqUl8ObSvRlmRx0XGcFNKZjQyappsQTNj3OTewc5R9wZIi+/I2ZUjUUKpxvMhNSQhoXmsaAHaF99C2bYDsh6Lgg7KNJUCwf5n6Yos6IT9t5tGoDzAjdlE0Jgr1vGrUsYYIaniDSSd/tEJ7BgkayJ2FoJljOP4KH4IH1f2Ixn6WAOCVhsriW6XX2T5Gy3F4TC1jVLTD3Ohc3PKPVtSk+ZW45oOESSmPaCqcDg+K6FjA1iDFGhfgDKWL+ymy/VD3ezRmygZxsmB2BUZ+kcx0zPvwqBdyr0q1wphUW/keH+UFftFDGxmpnqerVIrlT8g/ni4BjjTqQOkwd7ppez0KR2LEMbk5eEg5ATLV8Vmj/wRbspkehx+LsTuUGb8bIpVgarIopdRCVSBxi6IQliaO99o3KHu6bs9pEnFKAFQuXx/KmH976Z/e085ZDw/piW+3aym5ZexwAR5AovfqLApeLiNOCYCkgwqs8MVfFvQxVRX9hxBQWexG/mwzKEDj//oxAp9j9O+3GaNTH5gSGXFKAXgoAywmwP+uxAcY/FN8H/VU+eThwhwMeqDpw/4V7qsT4gNlst4/BSB5gC3kJMrt0oEYPdLXbGDPU1JfFI2ZHinzx8BWDpscgHDPU6jM92Pg9qh4ACYKmwxTOQDVwjdaq2VIn3cRQBVexcvZQA0THMqS+1Zdfs4pHEfqYpLEIieBuhFXwMbIb3T5GaebGItCCEoBhCpzaTEu7PWMKokBsSibcimA7BCH3u/KOj+D+pk8fiiabyWA7MBwVCaSUjTO06vDPhxrvqu6idUCqLxyPhxVN+5ptzNk1vAGV6kXziLATT9CXj7qn/ouhaGqQZXdk9jUSqAmVk4F95xRfsqeObqOXOE0NB46bQwgOzDExK/zJG0i1ZaSt8P/WqQw0UjukqoINk/c6m9Q7TLs49HGNo/2flcmdpJAPXjiWCxrputOOJ2pezotwOOcGzuRMoDicPoc7afmXDZqkdzxvwNtJYESTC40aTu4X5T1x5wnr8redQSPc+wsgRIoJY2WNYVt3OexgHzlbnmGpBhVMQCcfrFRADmocjDRpUeu4o9Frbn04k3YaRtHwXmVPcYBlC9SZyz8j7gfSyIhcThLqTxLlvy2ye8VQM1QfGQ6RnmE376lktK2wNHjvOosGTRGnoMAKDmNvTavegyNrSOxjsNYS3jVpUn7Jvkuyx8cQMkI7SUO1Qe4n8jLRx6844DtYMrLqX2kjivVzlu1uAoHwPy6v+Qk/T6f/wDXT/W+5H8dpQAAAABJRU5ErkJggg==" alt="">        
      </span>
      <span class="c_shareQQ c_shareBlock" data-shareId="QQ">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACP5JREFUeAHlXGFy00YU3pVToElmEn62A4M4AeYEOD8KtGmHcALcEyQ5Ae4JEk6AcwKSadMC/RFzAswJKg9M+xOYiU2TEm+/b20ZRZYdSftkm6CZRNJq973vfdrdt/u0a62meNz94x//xKhrnvlYNkb7WqmyhaOVjzP/okegjAqYYJRqam2Crp5r/vf1pVeNlcvvohkneQ3MkzsqB2+XL3Y6t4wxFaVVZUCYIwQSClYbWuvG0fz8i0kSOhEC7/76umI8/QDErcHIZUe+xhaHjnfQsau7ZufpT1cbYzMLPCyUwNu/vXmgtaoBpy+ANY+IwBhVe/7jlZ08hdOUESeQzfRCp7OulakCgJ8GxATyBOg5t4/mF3ekm7cogbd//3tNm+4WCPEnQEoeFYHR3ubzH77dzVM4qYwIgfSm3e7HLa30WpKSWUszyux63tzm0++/CVyxORPIWqe6J4+Ldg6uhsbL09kor/Sza2304oKz3MNJbKHJPvncyKONxEzstCGLzfG8uWpgz1G0D1C4HBf4Od5zHHk8v7CSx8FkJvC8kRe+8LwkZiLwu/03ZW3MwefYZEOixp3ZLxqtV/5cvdIcly/6LDWBtua1D/86r+SFpJDE44XF62mbcyonMmi2BU/DQiOmeWYFudBpH9DmNDhSEUiB58VhpCGFttLmNHnPJPDO/pvtL4m8kDTaTNvD+1Fn5Bt99KdmT0bnkH5i9pTRDTiq5r+Li81oP2QjOlqXlUYoTOl70ppHycPU7/64wfZIAu307OTjywk4jRZiedtHCwv1KGGjDGI6+6evOu01NJ8abq8xraiDTsUrzd0cNe0bSeCd394cKAQ9iwJGuZiT/nI8v7idlrgkLLf3X9eQvoF5+FLSc4k0zp2fr169nyQrkcCimy5idK+MVtUs460k8GFab3yq6og93gjTpM8I0K4kBWiHCGTzuNhpvwQAXxoE5ZG844WFikutS8LVG2od7qIm3kp6LpAWHM0v3IzjHvLCFzqHG1DmCygcEoGm8KII8qiIhqGZVfCK9oYUyyT4fW5OSRsiEB3T+qkcQje25s0vrsXfoJD4gRhEnavUNUgQvEDtfhAXd4pA9CXVIrwuat57r1QqnDwaxxfEWk6dcWMF7n1yFJVzikDcPIw+FLvWpeqoYYCYjoggkuh11VokSewyztGAQA5UocUX0zQQZPbGDUQH2YQveh6zkP7Q73NlEQ8I7GpdFbbBitPeHJ3SVI6idEe5sgT2Ig/mnrSV6Mx3Jtl04/j7uh/F093vP3FlCbzw4UOlCOcBx1FzB+smQXulMwMCWTWQK/sxDQUtgfi4Uskq5Oz8Zm+atS/E18Mg3xeGnFkCjVK3QoVSZ6NLdSlZrnKKwBJypvtTt7euIOPlMe25XPSgOa5z3D2CDu+kAw600bt0eFgepzjPM07ZZok82qAxxs5jy7gy5M7DVyhxAqFUHOw4Q9I8Q5NrpsmXJQ+58/Bq/CyF0uTFLKCRJt8k8xSCCdx5aG7yNXBuLpgkOWl08RNBmnxZ8pA764WzFEqTdxaGL3GcRfXJHjzTtbgyx/uWY/nCitO5yQrXS6yBvqRQgAwk5c2yLI1Pn/JN2OjZJbAAbKIEova9RzBzY1ZrDbERoyQ+UQIBzOkTpaRhSbL6jmQ76VneNBIo0unb2odvvHmBTKocv0NL1UJ+e+E4MBABb/RuUUMFEXx9IRYjsIrI1Fi1ICIIQvChfOZrX2irJFaOA5uh4LxnNgmpVQZ5MWQp18fq3HWROw/fgYMsypPyYjzUSEqf5TT0X+6YwZ3HpWSuhhYR6XDFdFZ5vHRnu8mdJzHJLiTScRYDjs8lKg658+iViloK4WjjTBcnZ+TOemEsC2tIouVnAtcdQJJ4uIaw9+lWTmrImSUQy1idCIxGtQnULkrXaoOrXOUg55NEDPCWD+Mr76OY80gOOUNf2jtcPrpwGazWXrUnyXA86PMa1XwHm52rvJ7WgZbAhZcP+voDfB3Z4LUxXaTn2z3PYRuW0i1TzqeBtMPovAfE7AIW/nrkUTiBT7MW2tr3iTxC8nsYzW5e8igEswbaaY8BgZ4x9X6a6MmcnDwWFZhBWFG6o1wNCOyv/21lwJcuq1bXpTvwNIqtTuhOkzdjnlZ0rfSAQArp9rYNZJSXnN1GPIzafLZ6xZ9GkIE6qRszrU2LJRlm5tQ4RzouAbtzAqRdi6dnuSdgo3RlVubHdhW/Mg1446UsdiTkbdmXEnlwqgYyHcbXI89zXQJofVbIowHEQky5jIkWMsMRpyECGXBEGae+EC/hXVTvLFwLYGpxN1XcliEC7dROexvxjNnuJ7eXLS0ubdz2j2CjTTWpLx/qA0NAd/ZfY6zjREQdq5c2k5SGOiZxpjfGxqEt6Krm12f2nq1eXUsqP5JADkLxWzDsO1w63gCzkRq+hu1lJZIdP5rHOpreMqI9j6JDhyRD4ml2Stlu38NgvoZnfvx52nvof4/fmCmPWm0xkkAqkNozx6mesgELr2nnkObkVASbZCldWtLdEx9qyzCab5vX0YMvAzOI3s/eqVEy7GrbbhneUGTZcu7triHy/qbj9fD+Czs/wrBlrD8YWwNDsjAhb6JW3Ajvv4Qz430IhJTPsnXICycVsFunCtp/lqRv2mkkjzanwZGqBlKQ7ZQ7h4GjU0mDadp5Whg9lNM6vVQ1kBZRIKdn9ErTtrBA/S3MdTNtikxdA0PQ/eFB47z1iWGzTVvzQj4yE8iC543EvOSRi1wEsiCPczLEOXOo0rM2+b8TgRRp94yZk/rn5lxsX65LVdetuKmdSDL/ShEApzoIhO2NyjN76WaPmF3Jo13ONTBKTv/3outIcwrIRmUKX7cYVck6rx6HQZRAKqKDudhuV/FqOAWaFSIz/zrSONKiz8QJjArnDzSgj6ghbVpEclxXQ0S6HsUleV0ogSFQNm27TV7jp+DdwmOhyJFn6xzw3ZafHiWb6iiFEyEwqrwfIqtg7IVwk0yAguM4yGowVCbhGKJ4z7qeOIFRQOwvuWXUrlPRykftsdEP1FIf+eLNvoXnAdLRvWJVLRY3Jv1MHp9P8vgfBP2dR2uS2V4AAAAASUVORK5CYII=" alt="">        
      </span>
      <div class="bshare-custom icon-medium" id="bshareBox">
        <div class="bsPromo bsPromo2"></div>
        <a class=" bshare-sinaminiblog" title="分享到新浪微博">
        </a>
        <span class="c_bshareWeibo">
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACtpJREFUeAHtXNtx20YUxYP/oSswUoGZCkz/ZiKbrkBUBZIrEDsQWYGoCsxYnvyaqcBwBaErCPnrEYics8DCF28QWFKkFMzQ+7q7uHtwH7t3V7atR3zC0ahvBcGrrW0PrDD0QssakB3btj0k/MlnFYbhihW2ZfkgWjlh6Fuu+81eLNaS8JB58HK4Jwbs9dayhgBgiDcrwAxw4OMDLB3LWgLQvw8J6EEADP/4YwgpOwdoI4DVNwBY1RBrgLmAdN7Znz8vqwhNtO0NQEpbsN2+wwsmYNQzwWyLMVYwC5Pep093Lfo26mIcQAK33W4v8fYr/PYtbY0mCaI1flPHcWam1dsogOHZ2QiqegNmPfyO8aHj+WDf3y9MMWcEQEidFwTBLbzn0BRj+xwH3nzpuu4FpHHV9T1wXN2eh7OzMVT266mAx9mSV/JM3rvNXi2p2g2hbF0Q3ICbzky048BQrzCcO677oa1tbKXCsaP4gikMDE3jsYfx4WDetAFxZxWmvYP4PyXw+PEGnBPntuuX3EkCw99/H2x7PYJ3LMuTXedbR792Hh7e2H/95dcR6vbGAD4D8DQma6jzb009dCMVVjav17vFG56q5GnwmHIj8JFzlpVl+VoJVOA9PZtXhoesb+RYaiVwy6XK0/G2EqC6/CCeeyVdJYBqoXnq67zK6dc0Yu51i+1SFaZL52odr2hkC2pYOeXmSqdSKoHc2/4Pnvru/RiLQiEoBJBRlVPa2xbOzGAlsSAmRUPmVDj2ulRdr6jDM65bxevDtcQgJ4Gwe1cg8CTR/3mFAH0CsUk9KQmMpe8fUPRTVMdbuMPWayq3XtwxBa47AMs0Q+8Ms752P316IcdMAUiXjZfSeZzEA5V6URVBiVcSE0zm3NSEEIy96N3fz/V4KRUGeNe64STSHz+8Kj65n4XEjBHGfw+6TRVt07YsRokExucZH5sOdCR0PMKch5HNVmYHE/KdILiTak1e42DIEtlfWO7yONvtG31kmgAYvH07x6DGRL01g2H4DVFuD/07TVSdewTBBwmkOp92nC+tefvZ8Y6SzaIC8JGdB1WLB+ELeVpG+4UF7LShI9gAMB+T8QD+S04sftYY90KOC0GZoO1aE7RMuTv5lfY3soFBMMRAj+F5Z2DE49eUk+SklP1y3TGylbYLwP2J/n0Y9qF7f+9xYuhzhx+fPo5Zb6m+UdGy0D7X+Q4p7/QM2V8BiJeoAisO8kBNsfz4DRO/qvKibKMqVvEECfVluwIeHwT9LuL6PqLoiW1nO+o1wLLrTnmNmfbCr3fq3Y14BkkZSNtUNVwWoALay4e3b8+hmpdS0uKlhgbKk1uxuo9S8I6iKoWZQ/uH1kTEiygN1dFOXVDqdhkPfYY5ekgw6v/mD222bVlzpFNI2tfg7OxW00NdJzqvJYZlHKovdX2HdEDsbIOeqYqXDVR22FTq5ECQrH9RBkZ5R6Pp4jksUFaem+ZBvwv916wn2LSTug/qQ51vm3I509s6zrDtAA37tQaP44PJ91av51faSlxjA4gjzOWL4slx+po3AOfDDCh103WmUkj1gDYweZmpgeU4mMCVlgZZ3zTPBWsVeHoctbANw++6rNN9gafGt20Py6RwoF9mOsXYqX2j6fFz49k21d2CZJwzhZpeM+UDIJcqg3+ks9F1bVJi12vTsWGfmdx0N+zTmizeDETCgLMMgDeWg0FSfF3euu5Q57umPXyZl10HyfWHKuHCziRXX1DBHYe+aB5rgzQpa/DnY/K8+/ytSpWxa2H4quANURWk8hrv8q0fP7i4viwl3KGB2NkmvFH2nXKznW1jOZYWTmKEXyQ1bKh5APDCDcOZ3shLcn4IBDwnqOOYyhvL9n3l9wFgstHOMq2AC4JrGKQx2qSkZUkrywrI6ILkOksYf5wr1Cf2L0tjsmwcwDLpi8NJH8G8VzgBLo6ju8xc9PUB8qtCup+VK6z33pd5+Ph9S5DvVRrNAgjbxw39zzlGuTjWeItSVuoYKJhygx/vUZOuQiXPk8p8pvL6xSE2CQ4Ckt/zfLWsse15tqeShOiYIA1eFFAYYms3yYLHMViHNhkUyA7NMu/1UV0Ln3ht+K2w0UQlnSXUZmViLI4B9V1mx8IBzw3q0uAhRAUv3WhrlwkKZIdn+bKoMqmz7UWSN5whdjoaY2boXm8lB6L0wdUPZR3zWJaMy5Yk7ENHIPtAxUulDHR9qrukP2Te4TrL1AuzqojoyCg3NsReBk85edjIdxoERlSgllPZLwab9rLN02/TqUkfYkcbuGpC3ISGRruODmKffDAeowKsf7CwXSD9yv6MpJRI3L9lY2c/XIbudaZsrgjsetziYAJGBg1s20sNFIZrFa0Tlfhqa11EnvZRP31GhtzPnye6QqdUaQDs6XImvcuUk6Lywjss1JOODTPEzmGoqCF9E7JzSYTjxaUsM49FsCfqUl8ObSvRlmRx0XGcFNKZjQyappsQTNj3OTewc5R9wZIi+/I2ZUjUUKpxvMhNSQhoXmsaAHaF99C2bYDsh6Lgg7KNJUCwf5n6Yos6IT9t5tGoDzAjdlE0Jgr1vGrUsYYIaniDSSd/tEJ7BgkayJ2FoJljOP4KH4IH1f2Ixn6WAOCVhsriW6XX2T5Gy3F4TC1jVLTD3Ohc3PKPVtSk+ZW45oOESSmPaCqcDg+K6FjA1iDFGhfgDKWL+ymy/VD3ezRmygZxsmB2BUZ+kcx0zPvwqBdyr0q1wphUW/keH+UFftFDGxmpnqerVIrlT8g/ni4BjjTqQOkwd7ppez0KR2LEMbk5eEg5ATLV8Vmj/wRbspkehx+LsTuUGb8bIpVgarIopdRCVSBxi6IQliaO99o3KHu6bs9pEnFKAFQuXx/KmH976Z/e085ZDw/piW+3aym5ZexwAR5AovfqLApeLiNOCYCkgwqs8MVfFvQxVRX9hxBQWexG/mwzKEDj//oxAp9j9O+3GaNTH5gSGXFKAXgoAywmwP+uxAcY/FN8H/VU+eThwhwMeqDpw/4V7qsT4gNlst4/BSB5gC3kJMrt0oEYPdLXbGDPU1JfFI2ZHinzx8BWDpscgHDPU6jM92Pg9qh4ACYKmwxTOQDVwjdaq2VIn3cRQBVexcvZQA0THMqS+1Zdfs4pHEfqYpLEIieBuhFXwMbIb3T5GaebGItCCEoBhCpzaTEu7PWMKokBsSibcimA7BCH3u/KOj+D+pk8fiiabyWA7MBwVCaSUjTO06vDPhxrvqu6idUCqLxyPhxVN+5ptzNk1vAGV6kXziLATT9CXj7qn/ouhaGqQZXdk9jUSqAmVk4F95xRfsqeObqOXOE0NB46bQwgOzDExK/zJG0i1ZaSt8P/WqQw0UjukqoINk/c6m9Q7TLs49HGNo/2flcmdpJAPXjiWCxrputOOJ2pezotwOOcGzuRMoDicPoc7afmXDZqkdzxvwNtJYESTC40aTu4X5T1x5wnr8redQSPc+wsgRIoJY2WNYVt3OexgHzlbnmGpBhVMQCcfrFRADmocjDRpUeu4o9Frbn04k3YaRtHwXmVPcYBlC9SZyz8j7gfSyIhcThLqTxLlvy2ye8VQM1QfGQ6RnmE376lktK2wNHjvOosGTRGnoMAKDmNvTavegyNrSOxjsNYS3jVpUn7Jvkuyx8cQMkI7SUO1Qe4n8jLRx6844DtYMrLqX2kjivVzlu1uAoHwPy6v+Qk/T6f/wDXT/W+5H8dpQAAAABJRU5ErkJggg==" alt="">
          </span>
      </div>
    </div>
    <!--正文结束 cms end->
 <!--彩通推广图-->
    <div class="ct_pushImg wrap15 c_pnCard">
      <img src="//www.sinaimg.cn/ty/temp/up/2017-05-16/U8314P6T64D100415F1091DT20170516093547.png" alt="" data-height="0.347">
    </div>
<!--评论模块-->
    <div id="comApp"></div>
 

    <!--广告卡片模块-->
    <div class="c_pushNewsCards_box">
      {{$adHtml}}
    </div>

    <!--精彩推荐模块-->
    <div  id="feedApp"></div>
  </div>

  <script type="text/javascript" src="http://i.sso.sina.com.cn/js/ssologin.js"></script>
  <script type="text/javascript" src="http://i.sso.sina.com.cn/js/outlogin_layer_mobile.js" charset="utf-8"></script>

  <!-- built files will be auto injected -->
<script type="text/javascript" src="//n.sinaimg.cn/sports/sina_lotto/ct_textPage/wap/js/manifest.js?v=a19"></script>
<script type="text/javascript" src="//n.sinaimg.cn/sports/sina_lotto/ct_textPage/wap/js/vendor.js?v=a19">
</script><script type="text/javascript" src="//n.sinaimg.cn/sports/sina_lotto/ct_textPage/wap/js/app.js?v=a19"></script>
</body>

</html>
{{endblock}}
<?php 

echo $html;
$api->cache()->set($cacheKey, $html, 300);
?>